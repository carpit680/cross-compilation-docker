<!-- @format -->

# cross-compilation-docker

Cross-compilation docker repository for ROS robot application using colcon for AWS Robomaker deployment to armhf or arm64.

This page describes in detail the entire process of building and bundling a robot application using colcon for AWS Robomaker deployment.

## Some Context

AWS Robomaker requires a ROS robot application bundle specific to the robot architecture and this bundle is created using colcon.

The robot in our case is either an armhf or an arm64(backward compatible with armhf and so this document focuses on armhf from now on) based and to build and bundle our application for armhf on an amd64 host we usually use AWS’s cross-compilation docker but it occasionally doesn’t work due to any buggy software updates and so, we have the option to follow the steps given below to set up the cross-compilation docker on any amd64 Linux host to be independent of the AWS platform for development and further reduce the costs. This comes with a downside of regularly having to update the repository manually but it kind of does give some reliability.

## Pre-requisites

- amd64 based Linux host

- ROS package

>**Note:** Use foxy-arm64 branch for ROS2 foxy on aarch64.

* All other pre-requisites are installed in the steps given below.

## Setup

1. Clone melodic-armhf branch of cross-compilation-docker repository.

```
    git clone -b melodic-armhf https://gitlab.com/carpit680/cross-compilation-docker.git
```

1. Go to the cross-compilation directory.

```
    cd cross-compilation-docker/
```

4. Build cross-compilation docker image and set up all the necessary components.

```
    sudo bash bin/build_image.bash
```

## Building, Bundling, and Deployment

Now that the cross-compilation docker is set up, we will start building, bundling, and deploying the robot application.

1. Run this command in your ROS workspace to start docker -> install dependencies using rosdep -> build -> bundle -> move robot application to S3 -> create new robot application version -> create new deployment job.

```
    sudo docker run -v $(pwd):/ws -it ros-cross-compile:armhf
```
