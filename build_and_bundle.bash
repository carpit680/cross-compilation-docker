#!/bin/bash

if [[ "$(whoami)" != root ]]; then
  echo "Do you even Root bro?"
  exit 1
fi

cd ws

apt update

echo "Installing dependencies."
rosdep install --from-paths src --ignore-src -r -y

echo "Installing markupsafe for the third time FFS!"
pip3 install -U markupsafe==2.0.0

echo "Building the ROS package."
colcon build --build-base armhf_build --install-base armhf_install

echo "Bundling the robot application."
colcon bundle --build-base armhf_build --install-base armhf_install --bundle-base armhf_bundle --apt-sources-list /opt/cross/apt-sources.yaml

echo "Renaming bundle archive."
mv armhf_bundle/output.tar armhf_bundle/foodlGo_robot_bundle.armhf.tar

echo "Uploading application bundle to S3 bucket."
aws s3 cp armhf_bundle/foodlGo_robot_bundle.armhf.tar s3://foodl-robomaker-bucket/foodlGo_robot_bundle.armhf.tar

echo "Creating a new robot application bundle."
dictionary=`aws robomaker create-robot-application-version --application arn:aws:robomaker:ap-northeast-1:948425589607:robot-application/foodlgo-robot-application/1636024190184`

echo "There is just no end to my BEEG BRAINS moments."
version=`python -c "print($dictionary['version'])"`

echo "Starting the application deployment to the Robot."
aws robomaker create-deployment-job --deployment-config concurrentDeploymentPercentage=1,failureThresholdPercentage=1 --fleet arn:aws:robomaker:ap-northeast-1:948425589607:deployment-fleet/foodlGo-mvp-fleet/1618323155310 --deployment-application-configs application=arn:aws:robomaker:ap-northeast-1:948425589607:robot-application/foodlgo-robot-application/1636024190184,applicationVersion=$version,launchConfig='{launchFile=deploy_prototype.launch,packageName=prototype}'

echo "Everything's wrapped up real tight now. Goodbye!"
exit 0